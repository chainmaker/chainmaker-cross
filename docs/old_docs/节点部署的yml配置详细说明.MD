## 节点部署的 yml 配置详细说明

### 1 前置需求
> 运行 install.sh 脚本 / 下载解压 release 包

### 2 部署时需要修改的参数

> SDK 配置文件

目前实现了 `chainmaker` 的跨链，因此至少准备两条链的 `SDK` 配置文档

比如以下两个 `SDK` 配置：
* SDK1，假设绝对路径为 /root/chainmaker_chain1_sdk.yml

* SDK2，假设绝对路径为 /root/chainmaker_chain2_sdk.yml

此时需要将 `release` 中的 `config/cross_chain.yml` 的 `adapters` 字段修改为：
```
adapters:
  - provider: chainmaker                            # 表示该链的类型，后面配置信息将是访问该链的配置信息
    chain_id: chain1                                # 该链的唯一ID标识
    config_path: /root/chainmaker_chain1_sdk.yml    # 该链对应Adapter的配置路径
  - provider: chainmaker                            # 表示该链的类型，后面配置信息将是访问该链的配置信息
    chain_id: chain2                                # 该链的唯一ID标识
    config_path: /root/chainmaker_chain2_sdk.yml    # 该链对应Adapter的配置路径
```

> SPV 配置文件

当前的跨链代理整合了 `chainmaker` 的 `SPV` 模块，不需要单独部署 `SPV` 节点。

比如以下两个 `SPV` 配置：
* SPV1，假设绝对路径为 /root/chainmaker_chain1_spv.yml

* SPV2，假设绝对路径为 /root/chainmaker_chain2_spv.yml

此时需要将 `release` 中的 `config/cross_chain.yml` 的 `provers` 字段修改为：
```
provers:
  - provider: spv                                   # 可提供证明的类型
    config_path: /root/chainmaker_chain1_spv.yml    # 该链对应的spv节点的配置路径
    chain_ids:                                      # 该证明类型下支持的链列表
      - chain1
  - provider: spv                                   # 可提供证明的类型
    config_path: /root/chainmaker_chain2_spv.yml    # 该链对应的spv节点的配置路径
    chain_ids:                                      # 该证明类型下支持的链列表
      - chain2
```

### 3 无法直连的 chainmaker 链

如果存在无法直连的 `chainmaker` 链，需要修改配置中的 `routers` 字段，使用 `p2p` 网络让转发节点转发跨链消息

比如 `chain3` 无法直连，已知网络中跨链代理C能够连接到 `chain3`，跨链代理C的 `libp2p` 地址为 `/ip4/192.168.1.100/tcp/19527/XXXXXX` 

那么需要修改 routers 为：
```
routers:
  - provider: libp2p                          # 远端跨链代理的网络访问方式
    libp2p:                                   # 远端跨链代理网络的具体信息
      address: /ip4/192.168.1.100/tcp/19527/XXXX   # 远端跨链代理基于libp2p访问下的地址
      protocol_id: /listener                  # P2p网络协议号
      delimit: '\n'                           # 发送到该跨链代理的消息处理分割符，通过该分割符对消息进行区分
      reconnect_limit: 10                     # router 连接断开重试次数
      reconnect_interval: 1000                # 连接间隔，单位毫秒
    chain_ids:                                # 远端跨链代理可直接操作的链集合，该集合为远端跨链代理adapters配置中支持的链列表
      - chain3
```

修改完成后，运行 start 脚本，查看输出