/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package event

import "go.uber.org/zap"

// Type is the base type for all event type by each event implements
type EventType byte

type MarshalType byte

// event types define
const (
	CrossEventType       EventType = iota // cross-chain event type, send from Client to Proxy
	CrossEventSearchType                  // cross-chain event type, send from Client to Proxy
	TransactionEventType                  // transaction event type, transaction messages connect each Proxy
	CrossTxType                           // para-chain event type, an interface, implied by each para-chain itself
	CrossRespEventType
	ProofRespEventType
	TransactionCtxEventType
	TxProofType

	BinaryMarshalType MarshalType = 0
	JsonMarshalType   MarshalType = 1
)

var log *zap.SugaredLogger

// InitLog init instance of log
func InitLog(zapLog *zap.SugaredLogger) {
	log = zapLog
}

// Event event interface of all instance event
type Event interface {
	// 返回消息实例类型
	GetType() EventType
}

// CrossTx tx of cross
type CrossTx struct {
	ChainID         string // 平行链ID
	Index           int    // 平行链消息的执行顺序
	ExecutePayload  []byte // 平行链上，执行时的交易体，转发到链上即可
	CommitPayload   []byte // 平行链上，确认时的交易体
	RollbackPayload []byte // 平行链上，回滚时的交易体
}

// NewCrossTx create new cross tx
func NewCrossTx(chainID string, index int, executePayload, commitPayload, rollbackPayload []byte) *CrossTx {
	return &CrossTx{
		ChainID:         chainID,
		Index:           index,
		ExecutePayload:  executePayload,
		CommitPayload:   commitPayload,
		RollbackPayload: rollbackPayload,
	}
}

// GetType return type of event
func (p *CrossTx) GetType() EventType {
	return CrossTxType
}

// GetChainID return chain-id
func (p *CrossTx) GetChainID() string {
	return p.ChainID
}

// GetExecutePayload return execute payload of cross tx
func (p *CrossTx) GetExecutePayload() []byte {
	return p.ExecutePayload
}

// GetCommitPayload return commit payload of cross tx
func (p *CrossTx) GetCommitPayload() []byte {
	return p.CommitPayload
}

// GetRollbackPayload return rollback payload of cross tx
func (p *CrossTx) GetRollbackPayload() []byte {
	return p.RollbackPayload
}

// CrossTxs cross tx struct
type CrossTxs struct {
	Events []*CrossTx
}

// NewCrossTxs create new CrossTxs instance by array of CrossTx
func NewCrossTxs(events []*CrossTx) *CrossTxs {
	return &CrossTxs{
		Events: events,
	}
}

// Len return length of array
func (ps *CrossTxs) Len() int {
	return len(ps.Events)
}

// Less compare object
func (ps *CrossTxs) Less(i, j int) bool {
	return ps.Events[i].Index < ps.Events[j].Index
}

// Swap swap object
func (ps *CrossTxs) Swap(i, j int) {
	ps.Events[i], ps.Events[j] = ps.Events[j], ps.Events[i]
}

// GetCrossTxs return cross txs
func (ps *CrossTxs) GetCrossTxs() []*CrossTx {
	return ps.Events
}
