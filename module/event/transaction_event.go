/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package event

type OpFuncType int32

const (
	ExecuteOpFunc  OpFuncType = 0
	CommitOpFunc   OpFuncType = 1
	RollbackOpFunc OpFuncType = -1
)

// TransactionEvent the struct of transaction event
type TransactionEvent struct {
	CrossID string     // 跨链消息的UUID
	OpFunc  OpFuncType // 操作类型
	ChainID string     // chainID
	Payload []byte     // 交易体数据
	TxProof *Proof     // 证明信息，不需要提供证明字段可以为nil
}

// NewExecuteTransactionEvent create new execute transaction event
func NewExecuteTransactionEvent(crossID, chainID string, payload []byte, txProof *Proof) *TransactionEvent {
	return &TransactionEvent{
		CrossID: crossID,
		OpFunc:  ExecuteOpFunc,
		ChainID: chainID,
		Payload: payload,
		TxProof: txProof,
	}
}

// NewRollbackTransactionEvent create new rollback transaction event
func NewRollbackTransactionEvent(crossID string, chainID string, payload []byte) *TransactionEvent {
	return &TransactionEvent{
		CrossID: crossID,
		OpFunc:  RollbackOpFunc,
		ChainID: chainID,
		Payload: payload,
	}
}

// NewCommitTransactionEvent create new commit transaction event
func NewCommitTransactionEvent(crossID string, chainID string, payload []byte) *TransactionEvent {
	return &TransactionEvent{
		CrossID: crossID,
		OpFunc:  CommitOpFunc,
		ChainID: chainID,
		Payload: payload,
	}
}

// GetType return type of this event
func (te *TransactionEvent) GetType() EventType {
	return TransactionEventType
}

// GetOpFunc return operation func
func (te *TransactionEvent) GetOpFunc() OpFuncType {
	return te.OpFunc
}

// GetChainID return chain-id
func (te *TransactionEvent) GetChainID() string {
	return te.ChainID
}

// GetPayload return payload of this event
func (te *TransactionEvent) GetPayload() []byte {
	return te.Payload
}
