module chainmaker.org/chainmaker-cross/adapter

go 1.15

require (
	chainmaker.org/chainmaker-cross/conf v0.0.0
	chainmaker.org/chainmaker-cross/event v0.0.0
	chainmaker.org/chainmaker-cross/logger v0.0.0
	chainmaker.org/chainmaker-cross/prover v0.0.0
	chainmaker.org/chainmaker-sdk-go v0.0.0-00010101000000-000000000000
	github.com/Rican7/retry v0.1.0
	github.com/golang/protobuf v1.5.2
	github.com/stretchr/testify v1.7.0
	go.uber.org/zap v1.16.0
)

replace (
	chainmaker.org/chainmaker-cross/conf => ../conf
	chainmaker.org/chainmaker-cross/event => ../event
	chainmaker.org/chainmaker-cross/logger => ../logger
	chainmaker.org/chainmaker-cross/prover => ../prover
	chainmaker.org/chainmaker-cross/utils => ../utils
	chainmaker.org/chainmaker-go/common => ../../recourse/chainmaker-sdk-go/common
	chainmaker.org/chainmaker-sdk-go => ../../recourse/chainmaker-sdk-go
	chainmaker.org/chainmaker-spv => ../../recourse/spv-prover
)
