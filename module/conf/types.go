/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package conf

import (
	"strconv"

	"chainmaker.org/chainmaker-cross/logger"
)

const (
	StringToByteIndex = 0
)

// LocalConf Local config struct
type LocalConf struct {
	ListenerConfig *ListenerConfig           `mapstructure:"listener"` // 本地服务配置
	AdapterConfigs []*AdapterConfig          `mapstructure:"adapters"` // 转接器配置
	RouterConfigs  []*RouterConfig           `mapstructure:"routers"`  // 路由配置
	ProverConfigs  []*ProverConfig           `mapstructure:"provers"`  // 证明器配置
	StorageConfig  *StorageConfig            `mapstructure:"storage"`  // 存储配置
	LogConfig      []*logger.LogModuleConfig `mapstructure:"log"`      // 日志配置
}

// ListenerConfig Listener config
type ListenerConfig struct {
	WebConfig     *WebConfig     `mapstructure:"web"`     // web服务配置
	ChannelConfig *ChannelConfig `mapstructure:"channel"` // P2p网络配置
	GrpcConfig    *GrpcConfig    `mapstructure:"grpc"`    // grpc服务配置
}

// WebConfig WebListener config
type WebConfig struct {
	Address string `mapstructure:"address"` // web服务监听地址
	Port    int    `mapstructure:"port"`    // web服务监听端口
}

// ToUrl return url of web config
func (webConfig *WebConfig) ToUrl() string {
	return webConfig.Address + ":" + strconv.Itoa(webConfig.Port)
}

// ChannelConfig ChannelListener config
type ChannelConfig struct {
	Provider      string               `mapstructure:"provider"` // P2p网络类型，如 libp2p，添加 Provider 需要扩展该类型
	LibP2PChannel *LibP2PChannelConfig `mapstructure:"libp2p"`   // libp2p 网络配置
}

// GrpcConfig Grpc config
type GrpcConfig struct {
	Network string `mapstructure:"protocol"`    // 网络类型
	Address string `mapstructure:"listen_port"` // 监听地址
}

// LibP2PChannelConfig LibP2P channel config
type LibP2PChannelConfig struct {
	Address     string `mapstructure:"address"`       // listen address
	PrivKeyFile string `mapstructure:"priv_key_file"` // p2p network peer id derived form private key
	ProtocolID  string `mapstructure:"protocol_id"`   // p2p network protocolID
	Delimit     string `mapstructure:"delimit"`       // p2p network delimit
}

// GetDelimit return delimit of libp2p connection
func (c *LibP2PChannelConfig) GetDelimit() byte {
	bs := []byte(c.Delimit)
	if len(bs) > 1 {
		panic("delimit config more than one rune")
	}
	return bs[StringToByteIndex]
}

// StorageConfig storage config
type StorageConfig struct {
	Provider string         `mapstructure:"provider"` // 存储类型
	LevelDB  *LevelDBConfig `mapstructure:"leveldb"`  // levelBD 配置
}

// LevelDBConfig leveldb config
type LevelDBConfig struct {
	StorePath       string `mapstructure:"store_path"` // 存储路径
	WriteBufferSize int    `mapstructure:"write_buffer_size"`
	BloomFilterBits int    `mapstructure:"bloom_filter_bits"`
}

// RouterConfig the config of router
type RouterConfig struct {
	Provider     string              `mapstructure:"provider"`  // 路由网络类型
	ChainIDs     []string            `mapstructure:"chain_ids"` // 代理节点能直连的链
	LibP2PRouter *LibP2PRouterConfig `mapstructure:"libp2p"`    // libp2p 网络配置
}

// LibP2PRouterConfig the config of libp2p router
type LibP2PRouterConfig struct {
	Address           string `mapstructure:"address"`            // libp2p 网络地址
	ProtocolID        string `mapstructure:"protocol_id"`        // p2p network protocolID
	Delimit           string `mapstructure:"delimit"`            // p2p network delimit
	ReconnectLimit    int    `mapstructure:"reconnect_limit"`    // 连接断开重试次数
	ReconnectInterval int    `mapstructure:"reconnect_interval"` // 连接间隔， 单位毫秒
}

// GetDelimit return delimit of libp2p router config
func (c *LibP2PRouterConfig) GetDelimit() byte {
	bs := []byte(c.Delimit)
	if len(bs) > 1 {
		panic("delimit config more than one rune")
	}
	return bs[StringToByteIndex]
}

// GetChainIDs return chain-ids of remote cross-chain proxy
func (r *RouterConfig) GetChainIDs() []string {
	return r.ChainIDs
}

// AdapterConfig adapter config
type AdapterConfig struct {
	Provider   string `mapstructure:"provider"`    // 转接器类型
	ChainID    string `mapstructure:"chain_id"`    // 转接器连接的链的ID
	ConfigPath string `mapstructure:"config_path"` // 配置路径
}

// ProverConfig prover config
type ProverConfig struct {
	Provider   string   `mapstructure:"provider"`    // 证明器类型，例如 spv，trust等
	ChainIDs   []string `mapstructure:"chain_ids"`   // 证明器连接的链的ID
	ConfigPath string   `mapstructure:"config_path"` // 证明器配置路径
}

// GetChainIDs return chain-ids of local provers
func (p *ProverConfig) GetChainIDs() []string {
	return p.ChainIDs
}
