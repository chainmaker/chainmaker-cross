module chainmaker.org/chainmaker-cross/sdk

go 1.16

require (
	chainmaker.org/chainmaker-cross/event v0.0.0
	chainmaker.org/chainmaker-cross/utils v0.0.0
	chainmaker.org/chainmaker-go/common v0.0.0
	chainmaker.org/chainmaker-sdk-go v0.0.0
	github.com/gogo/protobuf v1.3.2
	github.com/golang/protobuf v1.4.3
	github.com/google/uuid v1.2.0
	github.com/spf13/cobra v1.1.1
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.7.0
	go.uber.org/zap v1.16.0
)

replace (
	chainmaker.org/chainmaker-cross/event => ../../module/event
	chainmaker.org/chainmaker-cross/sdk => ./
	chainmaker.org/chainmaker-cross/utils => ../../module/utils
	chainmaker.org/chainmaker-go/common => ../../recourse/chainmaker-sdk-go/common
    chainmaker.org/chainmaker-sdk-go => ../../recourse/chainmaker-sdk-go
)
