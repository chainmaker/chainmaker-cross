/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package main

var (
	flagNameOfConfigFilepath          = "conf"
	flagNameShortHandOfConfigFilepath = "c"
	flagNameOfUrl                     = "url"
	flagNameShortHandOfUrl            = "u"
	flagNameOfCrossID                 = "crossID"
	flagNameOfParams                  = "params"
)

var (
	ConfigFilepath = "./cross_chain_sdk.yml"
	DefaultURL     = "http://localhost:8080"
)

type crossTxParam struct {
	ChainID        string            `mapstructure:"chain_id"`
	ContractName   string            `mapstructure:"contract_name"`
	ExecuteMethod  string            `mapstructure:"execute_method"`
	ExecuteParams  map[string]string `mapstructure:"execute_params"`
	RollbackMethod string            `mapstructure:"rollback_method"`
	RollbackParams map[string]string `mapstructure:"rollback_params"`
	Index          int               `mapstructure:"index"`
	ChainType      int               `mapstructure:"chain_type"`
}

type CrossTxParams struct {
	Params []*crossTxParam `mapstructure:"cross_tx_params"`
}
