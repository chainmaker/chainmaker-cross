/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package parallels

type ChainType int
type TxRequestType string

// ChainType chain type
const (
	ChainChainMaker ChainType = iota
)

const (
	TypeExecuteRequest  = "execute"
	TypeCommitRequest   = "commit"
	TypeRollbackRequest = "rollback"
)
