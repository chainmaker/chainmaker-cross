/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package parallels

import (
	"chainmaker.org/chainmaker-cross/event"
)

// MultiTx multi transactions
type MultiTx interface {

	// ToCrossTx convert MultiTx to CrossTx
	ToCrossTx() *event.CrossTx

	// Sign sign for MultiTx
	Sign() ([]byte, error)
}

// MultiTxBuilder the build for multi transactions
type MultiTxBuilder interface {
	// GetChainID return chain id
	GetChainID() string
	// GetClient return client
	GetClient() ClientInterface
	// UseParam load params
	UseParam(params ...TxRequestParams) MultiTxBuilder
	// WithOpts execute operation function
	WithOpts(opts ...MultiTxOpt) MultiTxBuilder
	// Final finish and create MultiTx
	Final(chainID string, Index int) MultiTx
}

// MultiTxOpt function for transaction operation
type MultiTxOpt func(MultiTxBuilder)
