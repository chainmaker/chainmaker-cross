/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package chainmaker

import (
	"fmt"
	"os"

	"chainmaker.org/chainmaker-cross/event"
	"chainmaker.org/chainmaker-cross/sdk/parallels"
	cmsdk "chainmaker.org/chainmaker-sdk-go"
	"go.uber.org/zap"
)

var _ parallels.ClientInterface = (*ChainClient)(nil)

// ChainClient chainmaker sdk client
type ChainClient struct {
	cmsdk.ChainClient
	Builder parallels.MultiTxBuilder
	chainID string
	logger  *zap.SugaredLogger
}

// NewClient create chainmaker chain client
func NewClient(chainId string, confPath string) (*ChainClient, error) {
	if confPath != "" && !Exists(confPath) {
		// check file exits
		return nil, fmt.Errorf("config path [%s] is invalid", confPath)
	}

	cmclient, err := cmsdk.NewChainClient(
		cmsdk.WithConfPath(confPath),
	)
	if err != nil {
		return nil, err
	}
	return &ChainClient{
		*cmclient,
		nil,
		chainId,
		nil,
	}, nil
}

// BuildTxRequestData imply sdk interface
func (cc *ChainClient) BuildTxRequestData(crossID, contractName, executeMethod string, executeParams map[string]string, rollbackMethod string, rollbackParams map[string]string, index int) (*event.CrossTx, error) {
	ret := &retData{}
	cc.Builder.WithOpts(BuildExecuteMultiTxOpt(ret, &ExecuteCallRequest{
		crossID,
		contractName,
		executeMethod,
		executeParams,
		contractName,
		rollbackMethod,
		rollbackParams,
	}),
		BuildCommitMultiTxOpt(ret, &CommitCallRequest{
			crossID,
		}),
		BuildRollbackMultiTxOpt(ret, &RollbackCallRequest{
			crossID,
		}),
	)
	if ret.Error != nil {
		return nil, ret.Error
	}
	return &event.CrossTx{
		ChainID:         cc.chainID,
		Index:           index,
		ExecutePayload:  ret.ExecutePayload,
		CommitPayload:   ret.CommitPayload,
		RollbackPayload: ret.RollbackPayload,
	}, nil
	// 生成 CrossTx
	//cc.Builder.UseParam(
	//	&ExecuteCallRequest{
	//		crossID,
	//		contractName,
	//		executeMethod,
	//		executeParams,
	//		contractName,
	//		rollbackMethod,
	//		rollbackParams,
	//	},
	//	&CommitCallRequest{
	//		crossID,
	//	},
	//	&RollbackCallRequest{
	//		crossID,
	//	},
	//)
	//tx := cc.Builder.WithOpts(
	//	BuildTxGetTxRequestExecute,
	//	BuildTxGetTxRequestCommit,
	//	BuildTxGetTxRequestRollback,
	//).Final(cc.Builder.GetChainID(), index)
	//return tx.ToCrossTx(), nil
}

// Exists check is exist for path
func Exists(path string) bool {
	_, err := os.Stat(path) //os.Stat获取文件信息
	if err != nil {
		return os.IsExist(err)
	}
	return true
}
