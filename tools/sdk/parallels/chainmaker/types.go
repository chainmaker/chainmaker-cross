/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package chainmaker

const (
	TransactionContractName   = "TransactionStable"
	TransactionExecuteMethod  = "Execute"
	TransactionCommitMethod   = "Commit"
	TransactionRollbackMethod = "Rollback"

	TransactionExecuteDataKey  = "executeData"
	TransactionRollbackDataKey = "rollbackData"

	BusinessCrossIDKey      = "crossID"
	BusinessContractNameKey = "contractName"
	BusinessMethodKey       = "method"
	BusinessParamsKey       = "params"
)
