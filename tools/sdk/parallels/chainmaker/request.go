/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package chainmaker

import (
	"chainmaker.org/chainmaker-cross/sdk/parallels"
)

// ExecuteCallRequest chainmaker execute payload
type ExecuteCallRequest struct {
	crossID              string
	executeContractName  string
	executeMethod        string
	executeParams        map[string]string
	rollbackContractName string
	rollbackMethod       string
	rollbackParams       map[string]string
}

// GetType return tx-request type
func (*ExecuteCallRequest) GetType() parallels.TxRequestType {
	return parallels.TypeExecuteRequest
}

// GetChainType return chain type
func (*ExecuteCallRequest) GetChainType() parallels.ChainType {
	return parallels.ChainChainMaker
}

// ExecuteCallRequest chainmaker commit payload
type CommitCallRequest struct {
	crossID string
}

// GetType return tx-request type
func (*CommitCallRequest) GetType() parallels.TxRequestType {
	return parallels.TypeCommitRequest
}

// GetChainType return chain type
func (*CommitCallRequest) GetChainType() parallels.ChainType {
	return parallels.ChainChainMaker
}

// ExecuteCallRequest - chainmaker rollback payload
type RollbackCallRequest struct {
	crossID string
}

// GetType return tx-request type
func (*RollbackCallRequest) GetType() parallels.TxRequestType {
	return parallels.TypeRollbackRequest
}

// GetChainType return chain type
func (*RollbackCallRequest) GetChainType() parallels.ChainType {
	return parallels.ChainChainMaker
}
