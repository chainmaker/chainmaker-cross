/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package sdk

import (
	"fmt"

	conf "chainmaker.org/chainmaker-cross/sdk/config"
	"chainmaker.org/chainmaker-cross/sdk/parallels"
	"chainmaker.org/chainmaker-cross/sdk/parallels/chainmaker"
)

// MultiTxFactory the factory function for MUltiTx
func MultiTxFactory(chainType parallels.ChainType, chainID string, configMap *conf.ConfigMap) (parallels.MultiTxBuilder, error) {
	ccConfig := configMap.GetCrossChainConfByChainID(chainID)
	if ccConfig == nil {
		return nil, fmt.Errorf("no cross chain config for chain: %s", chainID)
	}
	switch chainType {
	case parallels.ChainChainMaker:
		return newCMBuilder(chainID, ccConfig)
	default:
		return nil, fmt.Errorf("chain type unknown")
	}
}

// newCMBuilder create chainmaker crossTx builder
func newCMBuilder(chainID string, ccConfig *conf.CrossChainConf) (parallels.MultiTxBuilder, error) {
	// 创建 client 实例
	client, err := ClientFactory(parallels.ChainChainMaker, chainID, ccConfig.ChainClientConfigPath)
	if err != nil {
		return nil, err
	}
	cmclient, ok := client.(*chainmaker.ChainClient)
	if !ok {
		return nil, fmt.Errorf("chainID [%s] client type assert [*chainmaker.ChainClient] fail ", chainID)
	}
	if cmclient == nil {
		return nil, fmt.Errorf("chainID [%s] client is nil ", chainID)
	}
	builder := chainmaker.Builder{
		ChainID: chainID,
		Client:  cmclient,
		Config:  ccConfig,
		Params:  make(map[parallels.TxRequestType]parallels.TxRequestParams),
	}
	cmclient.Builder = &builder
	return &builder, nil
}
