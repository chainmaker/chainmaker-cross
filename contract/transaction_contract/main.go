/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package main

// 安装合约时会执行此方法，必须
//export init_contract
func initContract() {}

// 升级合约时会执行此方法，必须
//export upgrade
func upgrade() {}

//export Execute
func Execute() {
	// check and parse params
	crossID, executeParams, rollbackParams := UnpackUploadParams(Args())
	if crossID == EmptyCrossID {
		// will end contract calling
		ErrorResult("failed to get crossID")
		return
	} else {
		if isCrossIDExist(crossID) {
			ErrorResult("duplicated crossID: " + crossID)
			return
		}
		// check execute params
		if executeParams == nil {
			ErrorResult("executeParams is nil")
			return
		}
		eMap := callParamsToMap(executeParams)
		if eMap == nil {
			ErrorResult("failed to parse execute params")
			return
		}
		// check execute params
		if rollbackParams == nil {
			ErrorResult("rollbackParams is nil")
			return
		}
		rMap := callParamsToMap(rollbackParams)
		if rMap == nil {
			ErrorResult("failed to parse rollback params")
			return
		}
		// put data
		putExecute(crossID, eMap)
		putRollback(crossID, rMap)
	}

	// call execute method
	if bz, resultCode := CallContract(executeParams.ContractName, executeParams.Method, executeParams.Params); resultCode != SUCCESS {
		cdcs := ParamsMapToEasyCodecItem(executeParams.Params)
		paramsStr := EasyCodecItemToJsonStr(cdcs)
		// 返回失败结果
		putState(crossID, ExecuteFail)
		ErrorResult("failed to call contract, contractName: " + executeParams.ContractName + ", method: " + executeParams.Method + ", params: " + paramsStr)
		return
	} else {
		// 返回正确结果
		putState(crossID, ExecuteSuccess)
		SuccessResultByte(bz)
		return
	}
}

//export Commit
func Commit() {
	// get crossID
	if crossID, resultCode := Arg("crossID"); resultCode != SUCCESS {
		// 返回结果
		ErrorResult("failed to get crossID")
		return
	} else {
		// check state
		crossState := getState(crossID)
		if crossState == ExecuteSuccess {
			// change state
			putState(crossID, CommitSuccess)
			SuccessResult(string(CommitSuccess))
			return
		} else if crossState == ExecuteFail {
			ErrorResult(string("failed to Commit, unexpected pre-state: " + crossState))
			return
		} else if crossState == CommitSuccess {
			SuccessResultByte([]byte(CommitSuccess))
			return
		} else if crossState == CommitFail {
			// change state
			putState(crossID, CommitSuccess)
			SuccessResult(string(CommitSuccess))
			return
		} else if crossState == RollbackSuccess || crossState == RollbackFail || crossState == RollbackIgnore {
			ErrorResult(string("failed to Commit, unexpected pre-state: " + crossState))
			return
		}
		ErrorResult(string("failed to Commit, unexpected pre-state: " + crossState))
		return
	}
}

//export Rollback
func Rollback() {
	// get crossID
	if crossID, resultCode := Arg("crossID"); resultCode != SUCCESS {
		// 返回结果
		ErrorResult("failed to get crossID")
		return
	} else {
		// check state
		crossState := getState(crossID)
		// check rollback state
		if crossState == StateUnknown {
			// 返回结果
			putState(crossID, RollbackIgnore)
			SuccessResultByte([]byte("rollback ignored"))
			return
		}
		// check pre-state
		if crossState == ExecuteSuccess || crossState == RollbackFail {
			if m, resultCode := getRollback(crossID); resultCode != SUCCESS {
				ErrorResult("failed to get Rollback Data, crossID: " + crossID)
				return
			} else {
				cp := callParamsFromMap(m)
				if bz, resultCode := CallContract(cp.ContractName, cp.Method, cp.Params); resultCode != SUCCESS {
					cdcs := ParamsMapToEasyCodecItem(cp.Params)
					paramsStr := EasyCodecItemToJsonStr(cdcs)
					// 返回结果
					putState(crossID, RollbackFail)
					ErrorResult("failed to Rollback, contractName: " + cp.ContractName + ", method: " + cp.Method + ", params: " + paramsStr)
					return
				} else {
					// 返回结果
					putState(crossID, RollbackSuccess)
					SuccessResultByte(bz)
					return
				}
			}
		} else if crossState == ExecuteFail || crossState == RollbackIgnore {
			// 返回结果
			putState(crossID, RollbackIgnore)
			SuccessResultByte([]byte(RollbackIgnore))
			return
		} else if crossState == CommitSuccess || crossState == CommitFail {
			// 返回结果
			SuccessResultByte([]byte(RollbackIgnore))
			return
		} else if crossState == RollbackSuccess {
			SuccessResultByte([]byte(RollbackSuccess))
			return
		}
		ErrorResult(string("failed to Rollback, unexpected state: " + crossState))
		return
	}
}

//export ReadState
func ReadState() {
	// get crossID
	if crossID, resultCode := Arg("crossID"); resultCode != SUCCESS {
		// 返回结果
		ErrorResult("failed to get crossID")
		return
	} else {
		state := getState(crossID)
		SuccessResult(string(state))
		return
	}
}

//export ReadExecute
func ReadExecute() {
	// get crossID
	if crossID, resultCode := Arg("crossID"); resultCode != SUCCESS {
		// 返回结果
		ErrorResult("failed to get crossID")
		return
	} else {
		if m, resultCode := getExecute(crossID); resultCode != SUCCESS {
			ErrorResult("failed to get Execute Data by crossID: " + crossID)
			return
		} else {
			mapItems := ParamsMapToEasyCodecItem(m)
			SuccessResult(EasyCodecItemToJsonStr(mapItems))
			return
		}
	}
}

//export ReadRollback
func ReadRollback() {
	// get crossID
	if crossID, resultCode := Arg("crossID"); resultCode != SUCCESS {
		// 返回结果
		ErrorResult("failed to get crossID")
		return
	} else {
		if m, resultCode := getRollback(crossID); resultCode != SUCCESS {
			ErrorResult("failed to get Rollback Data by crossID: " + crossID)
			return
		} else {
			mapItems := ParamsMapToEasyCodecItem(m)
			SuccessResult(EasyCodecItemToJsonStr(mapItems))
			return
		}
	}
}

func main() {}
